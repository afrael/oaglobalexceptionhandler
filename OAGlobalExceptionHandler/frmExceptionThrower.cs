﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OAGlobalExceptionHandler
{
    public partial class frmExceptionThrower : Form
    {
        public frmExceptionThrower()
        {
            InitializeComponent();
        }

        private void btnThrowIt_Click(object sender, EventArgs e)
        {
            throwException();
        }

        private void throwException()
        {
            throw new Exception("Friendly Exception Error Message, if such thing exists!", new NotImplementedException("Inner Exception, without actual implmentation"));
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

    }
}
