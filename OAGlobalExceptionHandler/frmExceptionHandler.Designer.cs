﻿namespace OAGlobalExceptionHandler
{
    partial class frmExceptionHandler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExceptionHandler));
            this.pbIcon = new System.Windows.Forms.PictureBox();
            this.lblErrorText = new System.Windows.Forms.Label();
            this.txtExceptionDetails = new System.Windows.Forms.TextBox();
            this.lblShortMessage = new System.Windows.Forms.Label();
            this.btnCopy = new System.Windows.Forms.Button();
            this.btnContinue = new System.Windows.Forms.Button();
            this.btnQuit = new System.Windows.Forms.Button();
            this.btnScreenshot = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnViewDetails = new System.Windows.Forms.Button();
            this.lnklExceptionDetails = new System.Windows.Forms.LinkLabel();
            this.pnlWhiteBack = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pbIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // pbIcon
            // 
            this.pbIcon.BackColor = System.Drawing.Color.White;
            this.pbIcon.Location = new System.Drawing.Point(24, 17);
            this.pbIcon.Name = "pbIcon";
            this.pbIcon.Size = new System.Drawing.Size(32, 32);
            this.pbIcon.TabIndex = 0;
            this.pbIcon.TabStop = false;
            // 
            // lblErrorText
            // 
            this.lblErrorText.BackColor = System.Drawing.Color.White;
            this.lblErrorText.Location = new System.Drawing.Point(80, 12);
            this.lblErrorText.Name = "lblErrorText";
            this.lblErrorText.Size = new System.Drawing.Size(529, 49);
            this.lblErrorText.TabIndex = 1;
            this.lblErrorText.Text = resources.GetString("lblErrorText.Text");
            // 
            // txtExceptionDetails
            // 
            this.txtExceptionDetails.BackColor = System.Drawing.SystemColors.Control;
            this.txtExceptionDetails.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExceptionDetails.Location = new System.Drawing.Point(12, 150);
            this.txtExceptionDetails.Multiline = true;
            this.txtExceptionDetails.Name = "txtExceptionDetails";
            this.txtExceptionDetails.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtExceptionDetails.Size = new System.Drawing.Size(597, 293);
            this.txtExceptionDetails.TabIndex = 3;
            // 
            // lblShortMessage
            // 
            this.lblShortMessage.BackColor = System.Drawing.Color.White;
            this.lblShortMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShortMessage.Location = new System.Drawing.Point(12, 56);
            this.lblShortMessage.Name = "lblShortMessage";
            this.lblShortMessage.Size = new System.Drawing.Size(597, 55);
            this.lblShortMessage.TabIndex = 2;
            this.lblShortMessage.Text = "(short_exception_message)";
            this.lblShortMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblShortMessage.DoubleClick += new System.EventHandler(this.lblShortMessage_DoubleClick);
            // 
            // btnCopy
            // 
            this.btnCopy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCopy.Location = new System.Drawing.Point(9, 449);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(105, 23);
            this.btnCopy.TabIndex = 7;
            this.btnCopy.Text = "Copy to Clipboard";
            this.btnCopy.UseVisualStyleBackColor = true;
            this.btnCopy.Visible = false;
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // btnContinue
            // 
            this.btnContinue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnContinue.Location = new System.Drawing.Point(422, 449);
            this.btnContinue.Name = "btnContinue";
            this.btnContinue.Size = new System.Drawing.Size(90, 23);
            this.btnContinue.TabIndex = 1;
            this.btnContinue.Text = "Restart";
            this.btnContinue.UseVisualStyleBackColor = true;
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
            // 
            // btnQuit
            // 
            this.btnQuit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnQuit.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnQuit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuit.Location = new System.Drawing.Point(518, 449);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(90, 23);
            this.btnQuit.TabIndex = 0;
            this.btnQuit.Text = "Quit";
            this.btnQuit.UseVisualStyleBackColor = true;
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
            // 
            // btnScreenshot
            // 
            this.btnScreenshot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnScreenshot.Location = new System.Drawing.Point(311, 449);
            this.btnScreenshot.Name = "btnScreenshot";
            this.btnScreenshot.Size = new System.Drawing.Size(105, 23);
            this.btnScreenshot.TabIndex = 6;
            this.btnScreenshot.Text = "Grab Screenshot";
            this.btnScreenshot.UseVisualStyleBackColor = true;
            this.btnScreenshot.Click += new System.EventHandler(this.btnScreenshot_Click);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.Location = new System.Drawing.Point(169, 449);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(136, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "Ignore and Continue";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Visible = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnViewDetails
            // 
            this.btnViewDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewDetails.Location = new System.Drawing.Point(9, 449);
            this.btnViewDetails.Name = "btnViewDetails";
            this.btnViewDetails.Size = new System.Drawing.Size(90, 23);
            this.btnViewDetails.TabIndex = 8;
            this.btnViewDetails.Text = "View Details";
            this.btnViewDetails.UseVisualStyleBackColor = true;
            this.btnViewDetails.Visible = false;
            this.btnViewDetails.Click += new System.EventHandler(this.btnViewDetails_Click);
            // 
            // lnklExceptionDetails
            // 
            this.lnklExceptionDetails.AutoSize = true;
            this.lnklExceptionDetails.Location = new System.Drawing.Point(9, 129);
            this.lnklExceptionDetails.Name = "lnklExceptionDetails";
            this.lnklExceptionDetails.Size = new System.Drawing.Size(115, 13);
            this.lnklExceptionDetails.TabIndex = 9;
            this.lnklExceptionDetails.TabStop = true;
            this.lnklExceptionDetails.Text = "View Exception Details";
            this.lnklExceptionDetails.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // pnlWhiteBack
            // 
            this.pnlWhiteBack.BackColor = System.Drawing.Color.White;
            this.pnlWhiteBack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlWhiteBack.Location = new System.Drawing.Point(-3, -1);
            this.pnlWhiteBack.Name = "pnlWhiteBack";
            this.pnlWhiteBack.Size = new System.Drawing.Size(626, 121);
            this.pnlWhiteBack.TabIndex = 10;
            // 
            // frmExceptionHandler
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnOk;
            this.ClientSize = new System.Drawing.Size(621, 482);
            this.Controls.Add(this.lnklExceptionDetails);
            this.Controls.Add(this.btnViewDetails);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnScreenshot);
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.btnContinue);
            this.Controls.Add(this.btnCopy);
            this.Controls.Add(this.lblShortMessage);
            this.Controls.Add(this.txtExceptionDetails);
            this.Controls.Add(this.lblErrorText);
            this.Controls.Add(this.pbIcon);
            this.Controls.Add(this.pnlWhiteBack);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmExceptionHandler";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Application Error";
            this.Load += new System.EventHandler(this.frmExceptionHandler_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbIcon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbIcon;
        private System.Windows.Forms.Label lblErrorText;
        private System.Windows.Forms.TextBox txtExceptionDetails;
        private System.Windows.Forms.Label lblShortMessage;
        private System.Windows.Forms.Button btnCopy;
        private System.Windows.Forms.Button btnContinue;
        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.Button btnScreenshot;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnViewDetails;
        private System.Windows.Forms.LinkLabel lnklExceptionDetails;
        private System.Windows.Forms.Panel pnlWhiteBack;
    }
}