﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace OAGlobalExceptionHandler
{
    static class Program
    {

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // Add global exception catching
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmExceptionThrower());
        }

        #region Global Exception Handler

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            // A good place to publish your exceptions
            frmExceptionHandler.ShowGlobalExceptionHandlerDialog((Exception)e.ExceptionObject, true);
            Cursor.Current = Cursors.Arrow;
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            // A good place to publish your exceptions
            frmExceptionHandler.ShowGlobalExceptionHandlerDialog(e.Exception, true);
            Cursor.Current = Cursors.Arrow;
        }

        #endregion

    }
}
