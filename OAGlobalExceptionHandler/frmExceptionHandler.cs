﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace OAGlobalExceptionHandler
{
    public partial class frmExceptionHandler : Form
    {
        
        #region Enums, Ivars and Consts

        private Exception m_ex = null;
        private bool m_continueAllowed = true;
        private MessageBoxIcon m_mode = MessageBoxIcon.Error;
        private const int MAX_HEIGHT = 510;
        private const int MIN_HEIGHT = 195;

        private const string ERROR_MESSAGE_INTRO = "An unexpected error has occured and the application has stopped responding accordingly. You can try to continue using the application by clicking OK, or abort or restart the current process by clicking the Restart ANCP or Quit ANCP buttons.";
        private const string ERROR_MESSAGE_WARNING = "The current process could not be completed because of the following.";

        #endregion

        #region "Constructors"

        private frmExceptionHandler()
        {
            InitializeComponent();
        }

        private frmExceptionHandler(Exception exceptionToPublish, bool restartAllowed, MessageBoxIcon iconType) :
            this()
        {
            this.ExceptionToPublish = exceptionToPublish;
            this.AllowContinueButton = restartAllowed;
            this.m_mode = iconType;
            if (iconType == MessageBoxIcon.Asterisk)
                this.pbIcon.Image = System.Drawing.SystemIcons.Asterisk.ToBitmap();
            else if (iconType == MessageBoxIcon.Error)
                this.pbIcon.Image = System.Drawing.SystemIcons.Error.ToBitmap();
            else if (iconType == MessageBoxIcon.Exclamation)
                this.pbIcon.Image = System.Drawing.SystemIcons.Exclamation.ToBitmap();
            else if (iconType == MessageBoxIcon.Hand)
                this.pbIcon.Image = System.Drawing.SystemIcons.Hand.ToBitmap();
            else if (iconType == MessageBoxIcon.Information)
                this.pbIcon.Image = System.Drawing.SystemIcons.Information.ToBitmap();
            else if (iconType == MessageBoxIcon.None)
                this.pbIcon.Image = null;
            else if (iconType == MessageBoxIcon.Question)
                this.pbIcon.Image = System.Drawing.SystemIcons.Question.ToBitmap();
            else if (iconType == MessageBoxIcon.Stop)
                this.pbIcon.Image = System.Drawing.SystemIcons.Shield.ToBitmap();
            else if (iconType == MessageBoxIcon.Warning)
                this.pbIcon.Image = System.Drawing.SystemIcons.Warning.ToBitmap();
        }

        #endregion

        #region "Properties"

        private string ShortExceptionMessage
        {
            get 
            {
                if (this.ExceptionToPublish == null)
                    return "No Exception Message Available"; 
                else
                    return this.ExceptionToPublish.Message; 
            }
        }

        private Exception ExceptionToPublish
        {
            get { return m_ex; }
            set { m_ex = value; }
        }

        private bool AllowContinueButton
        {
            get { return m_continueAllowed; }
            set { m_continueAllowed = value; }
        }

        private MessageBoxIcon Mode
        {
            get { return m_mode; }
        }

        #endregion

        #region "Events"

        private void frmExceptionHandler_Load(object sender, EventArgs e)
        {
            System.Windows.Forms.Cursor.Current = Cursors.Default;
            this.lblShortMessage.Text = this.ShortExceptionMessage;
            this.btnContinue.Enabled = this.AllowContinueButton;
            this.txtExceptionDetails.Text = "Exception Occured at --> " + this.ExceptionToPublish.Source + "\r\n\r\n" + this.ExceptionToPublish.StackTrace;
            this.txtExceptionDetails.Visible = false;
            this.Height = MIN_HEIGHT;
            CopyTextToClipboard();
        }

        private void lblShortMessage_DoubleClick(object sender, EventArgs e)
        {
            CopyTextToClipboard();
        }

        private void btnQuit_Click(object sender, EventArgs e)
        {
            QuitApplication();
        }

        private void btnContinue_Click(object sender, EventArgs e)
        {
            RestartApplication();
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            CopyTextToClipboard();
        }

        private void btnScreenshot_Click(object sender, EventArgs e)
        {
            GrabScreenShot();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Application.UseWaitCursor = true;
            foreach (var item in Application.OpenForms)
                ((Form)item).Cursor = Cursors.Arrow;
            Cursor.Current = Cursors.Arrow;
            Application.DoEvents();
            this.Close();
        }

        private void btnViewDetails_Click(object sender, EventArgs e)
        {
            ExpandOrContractDetails();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ExpandOrContractDetails();
        }

        #endregion

        #region "Methods"

        public static void ShowGlobalExceptionHandlerDialog(Exception exceptionToPublish, bool allowApplicationRestart)
        {
            frmExceptionHandler dlg = new frmExceptionHandler(exceptionToPublish, allowApplicationRestart, MessageBoxIcon.Error);
            dlg.ShowDialog();
        }

        public static void ShowGlobalExceptionHandlerDialog(Exception exceptionToPublish, bool allowApplicationRestart, MessageBoxIcon iconType)
        {
            frmExceptionHandler dlg = new frmExceptionHandler(exceptionToPublish, allowApplicationRestart, iconType);
            dlg.ShowDialog();
        }

        private void QuitApplication()
        {
            //if (Application.AllowQuit)
            //{
                if (MessageBox.Show("Please confirm that you wish to Quit the Application.", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    Application.Exit();
            //}
            //else
            //    MessageBox.Show("Quitting the Application is not Allowed, please contact a System Administrator.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void RestartApplication()
        {
            if (MessageBox.Show("Please confirm that you wish to Restart the Application.", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                Application.Restart();
        }

        private void CopyTextToClipboard()
        {
            System.Windows.Forms.Clipboard.SetText("Exception --> " + this.lblShortMessage.Text + "\r\n\r\nException Details --> " + this.txtExceptionDetails.Text);
        }

        private void GrabScreenShot()
        {
            Bitmap bitmap = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            Graphics graphics = Graphics.FromImage(bitmap as Image);
            graphics.CopyFromScreen(0, 0, 0, 0, bitmap.Size);
            System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            dlg.Description = "Please select the folder where the screenshot will be stored.";
            dlg.ShowDialog();
            Bitmap bm2 = new Bitmap(bitmap);
            bitmap.Dispose();
            bm2.Save(dlg.SelectedPath + @"\exc-screenshot-" + DateTime.Now.ToFileTimeUtc().ToString() + ".png", ImageFormat.Png);
            bm2.Dispose();
            GC.Collect();
         }

        private void ExpandOrContractDetails()
        {
            if (this.Height == MIN_HEIGHT)
            {
                this.Height = MAX_HEIGHT;
                this.txtExceptionDetails.Visible = true;
                this.lnklExceptionDetails.Text = "Hide Exception Details";
                this.btnViewDetails.Text = "Hide Exception Details";
            }
            else if (this.Height == MAX_HEIGHT)
            {
                this.Height = MIN_HEIGHT;
                this.txtExceptionDetails.Visible = false;
                this.lnklExceptionDetails.Text = "View Exception Details";
                this.btnViewDetails.Text = "View Exception Details";
            }
        }

        #endregion

    }
}
